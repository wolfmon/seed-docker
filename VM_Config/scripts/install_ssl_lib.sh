#!/bin/bash

# SSL Lib
apt-get install -qq -y libssl-dev
apt-get install -qq -y openssl-dev

# Openssl Source

mkdir /home/seed/source/openssl
wget -P /home/seed/source/openssl https://www.openssl.org/source/old/1.0.2/openssl-1.0.2g.tar.gz
tar -xvzf /home/seed/source/openssl/openssl-1.0.2g.tar.gz -C /home/seed/source/openssl
rm /home/seed/source/openssl/openssl-1.0.2g.tar.gz

