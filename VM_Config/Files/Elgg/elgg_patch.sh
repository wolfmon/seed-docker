#!/bin/bash

## CSRF Patch
cp ./Security_Patch_Files/CSRF/ActionsService.php /var/www/CSRF/Elgg/vendor/elgg/elgg/engine/classes/Elgg/

## XSS Patch
cp ./Security_Patch_Files/XSS/dropdown.php  /var/www/XSS/Elgg/vendor/elgg/elgg/views/default/output/
cp ./Security_Patch_Files/XSS/email.php  /var/www/XSS/Elgg/vendor/elgg/elgg/views/default/output/
cp ./Security_Patch_Files/XSS/text.php  /var/www/XSS/Elgg/vendor/elgg/elgg/views/default/output/
cp ./Security_Patch_Files/XSS/url.php  /var/www/XSS/Elgg/vendor/elgg/elgg/views/default/output/



