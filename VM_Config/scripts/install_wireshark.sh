#!/bin/bash

apt-get -qq -y install wireshark

chgrp seed /usr/bin/dumpcap
chmod 750 /usr/bin/dumpcap
setcap cap_net_raw,cap_net_admin+eip /usr/
apt-get install -y netwox
apt-get install -y libpcap-dev
