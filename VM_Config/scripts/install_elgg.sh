#/bin/bash

echo 
echo "================================"
echo "Installing Elgg"
echo "================================"
echo

sudo mkdir -p /var/elgg/xss
sudo mkdir -p /var/elgg/csrf

sudo chmod 777 /var/elgg/xss
sudo chmod 777 /var/elgg/csrf

mysql -u root -pseedubuntu < ../Files/elgg.sql

# XSS Elgg Setup

unzip ../Files/Elgg/elgg-2.2.2.zip -d ../Files/Elgg/
sudo mkdir -p /var/www/XSS/Elgg/
sudo mv ../Files/Elgg/elgg-2.2.2/* /var/www/XSS/Elgg/
sudo rm -rf ../Files/Elgg/elgg-2.2.2/

sudo chmod 777 /var/www/XSS/
sudo chmod 777 /var/www/XSS/Elgg/
sudo chmod 777 /var/www/XSS/Elgg/elgg-config/

if grep -q "127.0.0.1       www.xsslabelgg.com" /etc/hosts; then
    echo "SEED XSS Elgg lab local host already set" 
else
    sudo sh -c "echo '127.0.0.1       www.xsslabelgg.com' >> /etc/hosts"
fi
if grep -q "http://www.xsslabelgg.com" /etc/apache2/sites-available/000-default.conf; then
    echo "SEED XSS Elgg lab virtual host already set"
else
    sudo sh -c "echo '<VirtualHost *:80>' >> /etc/apache2/sites-available/000-default.conf"
    sudo sh -c "echo '        ServerName http://www.xsslabelgg.com' >> /etc/apache2/sites-available/000-default.conf"
    sudo sh -c "echo '        DocumentRoot /var/www/XSS/Elgg' >> /etc/apache2/sites-available/000-default.conf"
    sudo sh -c "echo '</VirtualHost>' >> /etc/apache2/sites-available/000-default.conf"
    sudo service apache2 restart
fi

# CSRF Elgg Setup

unzip ../Files/Elgg/elgg-2.2.2.zip -d ../Files/Elgg/
sudo mkdir -p /var/www/CSRF/Elgg/
sudo mv ../Files/Elgg/elgg-2.2.2/* /var/www/CSRF/Elgg/
sudo rm -rf ../Files/Elgg/elgg-2.2.2/

sudo chmod 777 /var/www/CSRF/
sudo chmod 777 /var/www/CSRF/Elgg/
sudo chmod 777 /var/www/CSRF/Elgg/elgg-config/

if grep -q "127.0.0.1       www.csrflabelgg.com" /etc/hosts; then
    echo "SEED CSRF Elgg lab local host already set" 
else
    sudo sh -c "echo '127.0.0.1       www.csrflabelgg.com' >> /etc/hosts"
fi
if grep -q "http://www.csrflabelgg.com" /etc/apache2/sites-available/000-default.conf; then
    echo "SEED CSRF Elgg lab virtual host already set"
else
    sudo sh -c "echo '<VirtualHost *:80>' >> /etc/apache2/sites-available/000-default.conf"
    sudo sh -c "echo '        ServerName http://www.csrflabelgg.com' >> /etc/apache2/sites-available/000-default.conf"
    sudo sh -c "echo '        DocumentRoot /var/www/CSRF/Elgg' >> /etc/apache2/sites-available/000-default.conf"
    sudo sh -c "echo '</VirtualHost>' >> /etc/apache2/sites-available/000-default.conf"
    sudo service apache2 restart
fi

clear
echo "Please run 'sudo apt-get install phpmyadmin' manually. There are issues when it is run from a script. Please use 'seedubuntu' as admin password".
echo "Select apache2 as the web server when asked. Also select 'Yes' when asked about database generation."
echo
#read-p "Press Enter to acknowledge." CONT

# Common Elgg Instructions
echo
echo
echo "Databases for Elgg installation have been setup."
echo
echo "Please install the XSS elgg website by visiting the URL http://www.xsslabelgg.com in the browser"
echo "Please install the CSRF elgg website by visiting the URL http://www.csrflabelgg.com in the browser"
echo
echo "Please check the file 'Files/Elgg/Elgg_install_instructions' for installation instructions."
echo
#read-p "Press Enter to acknowledge." CONT

