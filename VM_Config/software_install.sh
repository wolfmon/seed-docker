#!/bin/bash



echo
echo "==================================="
echo "Fetch VM Customization Zip File"
echo "==================================="
echo

wget -P ./Files/ http://www.cis.syr.edu/~wedu/seed/Documentation/Ubuntu12_04_VM/Customization.zip

echo
echo "==================================="
echo "Additional Entries in /etc/hosts"
echo "==================================="
echo

if grep -q "127.0.0.1       User" /etc/hosts; then
    echo "User local host already set" 
else
    sudo sh -c "echo '127.0.0.1       User' >> /etc/hosts"
fi

if grep -q "127.0.0.1       Attacker" /etc/hosts; then
    echo "Attacker local host already set" 
else
    sudo sh -c "echo '127.0.0.1       Attacker' >> /etc/hosts"
fi

if grep -q "127.0.0.1       Server" /etc/hosts; then
    echo "Server local host already set" 
else
    sudo sh -c "echo '127.0.0.1       Server' >> /etc/hosts"
fi





# # Install Sublime Text Editor
# wget -qO - https://download.sublimetext.com/sublimehq-pub.gpg | sudo apt-key add -
# echo "deb https://download.sublimetext.com/ apt/stable/" | sudo tee /etc/apt/sources.list.d/sublime-text.list
# echo | sudo apt-get update && apt-get install -y sublime-text

echo
echo "================================"
echo "Installing Hex Editors"
echo "================================"
echo

# Hex Editor
echo | sudo apt-get install bless
sudo mkdir /var/bless
sudo chmod 777 /var/bless
cp Files/preferences.xml ~/.config/bless/

echo | sudo apt-get install ghex

echo
echo "================================"
echo "Installing SSL Lib"
echo "================================"
echo

# SSL Lib
echo | sudo apt-get install libssl-dev
echo | sudo apt-get install openssl-dev

# Openssl Source

mkdir /home/seed/source/openssl
wget -P /home/seed/source/openssl https://www.openssl.org/source/old/1.0.2/openssl-1.0.2g.tar.gz
tar -xvzf /home/seed/source/openssl/openssl-1.0.2g.tar.gz -C /home/seed/source/openssl
rm /home/seed/source/openssl/openssl-1.0.2g.tar.gz

echo
echo "================================"
echo "Installing Telnet, SSH, FTP"
echo "================================"
echo

# Telnet, SSH, FTP
echo | sudo apt-get install openbsd-inetd  
echo | sudo apt-get install telnetd
echo | sudo apt-get install openssh-server
echo | sudo apt-get install vsftpd

echo
echo "================================"
echo "Installing BIND"
echo "================================"
echo

# DNS
echo | sudo apt-get install bind9
echo | sudo apt-get install libnet1-dev

# BIND config

echo | sudo cp ./Files/named.conf.options /etc/bind
echo | sudo chown bind /etc/bind/named.conf.options
echo | sudo touch /var/cache/bind/dump.db
echo | sudo chown bind /var/cache/bind/dump.db
echo | sudo chgrp bind /var/cache/bind/dump.db
echo | sudo service bind9 restart

# Apache/PHP/MySQL

echo | clear
echo "================================"
echo "Installing Apache/PHP/MySQL"
echo "================================"


echo | sudo apt-get install apache2
echo | sudo cp ./Files/apache2.conf /etc/apache2/
echo | sudo service apache2 restart
echo | sudo a2enmod rewrite
echo | sudo service apache2 restart

sudo debconf-set-selections <<< "mysql-server mysql-server/root_password password seedubuntu"
sudo debconf-set-selections <<< "mysql-server mysql-server/root_password_again password seedubuntu"



echo | sudo apt-get -y -qq install mysql-server
echo | sudo apt-get install -y php
echo | sudo apt-get install -y libapache2-mod-php
echo | sudo apt-get install -y php-mysqlnd

echo | sudo service apache2 restart

echo 
echo "================================"
echo "Installing Networking Tools"
echo "================================"
echo 

#Networking Tools
clear
echo "About to install wireshark. Make sure to select 'No' when asked whether non-superuser should be able to capture packets."
sudo DEBIAN_FRONTEND=noninteractive apt-get -y install wireshark
echo "wireshark-common wireshark-common/install-setuid boolean false" | sudo debconf-set-selections
sudo DEBIAN_FRONTEND=noninteractive dpkg-reconfigure wireshark-common

echo | sudo apt-get -qq -y install wireshark
echo | sudo chgrp seed /usr/bin/dumpcap  
echo | sudo chmod 750 /usr/bin/dumpcap  
echo | sudo setcap cap_net_raw,cap_net_admin+eip /usr/bin/dumpcap

echo | sudo apt-get install netwox
echo | sudo apt-get install libpcap-dev

echo 
echo "================================"
echo "Installing Shells"
echo "================================"
echo 

# Shell 
echo | sudo apt-get install zsh

echo 
echo "================================"
echo "Installing SQL Injection Patch"
echo "================================"
echo 

chmod a+x ./Files/sql_injection_patch/bootstrap.sh
echo | ./Files/sql_injection_patch/bootstrap.sh

echo 
echo "================================"
echo "Installing GIT"
echo "================================"
echo 

echo | sudo apt-get install git

echo 
echo "================================"
echo "Installing lab specific tools"
echo "================================"
echo

# Shellnoob - assist in writing shellcode
git clone --recursive https://github.com/reyammer/shellnoob.git /home/seed/source/shellnoob

echo 
echo "================================"
echo "Installing ROP Lab Tools"
echo "================================"
echo

cwd=$(pwd)

#ROPgadget
echo | sudo apt-get install python-pip
echo | sudo pip install capstone
git clone https://github.com/JonathanSalwan/ROPgadget.git /home/seed/source/ropgadget
cd /home/seed/source/ropgadget
sudo python setup.py install

cd $cwd

#gdb-peda
cwd=$(pwd)
git clone https://github.com/longld/peda.git /home/seed/source/gdbpeda
echo "source /home/seed/source/gdbpeda/peda.py" >> ~/.gdbinit

cd $cwd

echo 
echo "================================"
echo "Installing C++ Boost"
echo "================================"
echo

cwd=$(pwd)

mkdir /home/seed/source
wget -P /home/seed/source/ https://dl.bintray.com/boostorg/release/1.64.0/source/boost_1_64_0.tar.bz2
cd /home/seed/source/
tar --bzip2 -xvf boost_1_64_0.tar.bz2
cd boost_1_64_0/
./bootstrap.sh --prefix=/home/seed/source/boost_1_64_0/
./b2
rm /home/seed/source/boost_1_64_0.tar.bz2

cd $cwd


cwd=$(pwd)

mkdir /home/seed/source/md5tool
wget -P /home/seed/source/md5tool https://www.win.tue.nl/hashclash/fastcoll_v1.0.0.5-1_source.zip
unzip /home/seed/source/md5tool/fastcoll_v1.0.0.5-1_source.zip -d /home/seed/source/md5tool/md5collgen/
cd /home/seed/source/md5tool/md5collgen
g++ -c md5.cpp
g++ -c block0.cpp
g++ -c block1.cpp
g++ -c block1stevens00.cpp
g++ -c block1stevens01.cpp
g++ -c block1stevens11.cpp
g++ -c block1stevens10.cpp
g++ -c block1wang.cpp
export LD_LIBRARY_PATH=/home/seed/source/boost_1_64_0/stage/lib:$LD_LIBRARY_PATH
g++ -o md5collgen main.cpp -I /home/seed/source/boost_1_64_0 -L /home/seed/source/boost_1_64_0/stage/lib -lboost_thread -lboost_serialization -lboost_program_options -lboost_filesystem -lboost_iostreams -lboost_system md5.o block0.o block1.o block1stevens00.o block1stevens01.o block1stevens10.o block1stevens11.o block1wang.o
rm /home/seed/source/md5tool/fastcoll_v1.0.0.5-1_source.zip

mkdir -p /home/seed/bin/md5tool/
cp /home/seed/source/md5tool/md5collgen/md5collgen /home/seed/bin/md5tool/

var=/home/seed/source/boost_1_64_0/stage/lib:$LD_LIBRARY_PATH
str="LD_LIBRARY_PATH="
quote="\""
str2=$str$quote$var$quote
export str2
sudo -E bash -c 'echo -e "$str2" >> /etc/environment'


cd $cwd

echo 
echo "================================"
echo "Change bashrc"
echo "================================"
echo

cp ./Files/bashrc ~/.bashrc
 

clear
echo 
echo "================================"
echo "Installing Elgg"
echo "================================"
echo

sudo mkdir -p /var/elgg/xss
sudo mkdir -p /var/elgg/csrf

sudo chmod 777 /var/elgg/xss
sudo chmod 777 /var/elgg/csrf

mysql -u root -pseedubuntu < ./Files/elgg.sql

# XSS Elgg Setup

unzip ./Files/Elgg/elgg-2.2.2.zip -d ./Files/Elgg/
sudo mkdir -p /var/www/XSS/Elgg/
sudo mv ./Files/Elgg/elgg-2.2.2/* /var/www/XSS/Elgg/
sudo rm -rf ./Files/Elgg/elgg-2.2.2/

sudo chmod 777 /var/www/XSS/
sudo chmod 777 /var/www/XSS/Elgg/
sudo chmod 777 /var/www/XSS/Elgg/elgg-config/

if grep -q "127.0.0.1       www.xsslabelgg.com" /etc/hosts; then
    echo "SEED XSS Elgg lab local host already set" 
else
    sudo sh -c "echo '127.0.0.1       www.xsslabelgg.com' >> /etc/hosts"
fi
if grep -q "http://www.xsslabelgg.com" /etc/apache2/sites-available/000-default.conf; then
    echo "SEED XSS Elgg lab virtual host already set"
else
    sudo sh -c "echo '<VirtualHost *:80>' >> /etc/apache2/sites-available/000-default.conf"
    sudo sh -c "echo '        ServerName http://www.xsslabelgg.com' >> /etc/apache2/sites-available/000-default.conf"
    sudo sh -c "echo '        DocumentRoot /var/www/XSS/Elgg' >> /etc/apache2/sites-available/000-default.conf"
    sudo sh -c "echo '</VirtualHost>' >> /etc/apache2/sites-available/000-default.conf"
    sudo service apache2 restart
fi

# CSRF Elgg Setup

unzip ./Files/Elgg/elgg-2.2.2.zip -d ./Files/Elgg/
sudo mkdir -p /var/www/CSRF/Elgg/
sudo mv ./Files/Elgg/elgg-2.2.2/* /var/www/CSRF/Elgg/
sudo rm -rf ./Files/Elgg/elgg-2.2.2/

sudo chmod 777 /var/www/CSRF/
sudo chmod 777 /var/www/CSRF/Elgg/
sudo chmod 777 /var/www/CSRF/Elgg/elgg-config/

if grep -q "127.0.0.1       www.csrflabelgg.com" /etc/hosts; then
    echo "SEED CSRF Elgg lab local host already set" 
else
    sudo sh -c "echo '127.0.0.1       www.csrflabelgg.com' >> /etc/hosts"
fi
if grep -q "http://www.csrflabelgg.com" /etc/apache2/sites-available/000-default.conf; then
    echo "SEED CSRF Elgg lab virtual host already set"
else
    sudo sh -c "echo '<VirtualHost *:80>' >> /etc/apache2/sites-available/000-default.conf"
    sudo sh -c "echo '        ServerName http://www.csrflabelgg.com' >> /etc/apache2/sites-available/000-default.conf"
    sudo sh -c "echo '        DocumentRoot /var/www/CSRF/Elgg' >> /etc/apache2/sites-available/000-default.conf"
    sudo sh -c "echo '</VirtualHost>' >> /etc/apache2/sites-available/000-default.conf"
    sudo service apache2 restart
fi

clear
echo "Please run 'sudo apt-get install phpmyadmin' manually. There are issues when it is run from a script. Please use 'seedubuntu' as admin password".
echo "Select apache2 as the web server when asked. Also select 'Yes' when asked about database generation."
echo


# Common Elgg Instructions
echo
echo
echo "Databases for Elgg installation have been setup."
echo
echo "Please install the XSS elgg website by visiting the URL http://www.xsslabelgg.com in the browser"
echo "Please install the CSRF elgg website by visiting the URL http://www.csrflabelgg.com in the browser"
echo
echo "Please check the file 'Files/Elgg/Elgg_install_instructions' for installation instructions."
echo





