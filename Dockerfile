###########################################
#----START OF Ubuntu Core Configuration----
###########################################
FROM ubuntu:16.04


#Make FrontEnd non-interactive, supress confirmation prompt
ENV DEBIAN_FRONTEND noninteractive
#Make directories according to existing installation script

RUN mkdir -p /seed-setup/customization /home/seed/source/ /home/seed/bin /usr/seed/

#Install basic packages
RUN apt-get update && apt-get install -y \
	build-essential \
	cmake \
	git \
	curl \
	wget \
	sudo \
	apt-transport-https \
	apache2-utils \
	openssl \
	ssh \
	openssh-server \
	tree \
	apt-utils \
	openbsd-inetd  \
	telnetd	\
	openssh-server	\
	vsftpd	\
	expect \
	zsh \
	unzip \
	&& apt-get autoclean -y && apt-get autoremove -y


#Add seed user into container
RUN useradd -d /home/seed -p $(openssl passwd -1 dees) seed && \
	usermod -aG sudo seed

#########################################
#----END OF Ubuntu Core Configuration----
#########################################

########################################
#----START OF SEED Lab Customization----
########################################

ARG CMISS
WORKDIR /seed-setup/customization
RUN git clone https://gitlab.com/wolfmon/seed-docker.git && echo $CMISS
RUN tree 
WORKDIR /seed-setup/customization/seed-docker/VM_Config

#Fetch VM Customization Zip File
RUN wget -P ./Files/ http://www.cis.syr.edu/~wedu/seed/Documentation/Ubuntu12_04_VM/Customization.zip



WORKDIR /seed-setup/customization/seed-docker/VM_Config/scripts
# RUN pwd
RUN chmod +x /seed-setup/customization/seed-docker/VM_Config/scripts/*.sh

RUN /seed-setup/customization/seed-docker/VM_Config/scripts/config-seed.sh && echo $CMISS











# #Install GUI Environment




# #OPTION 1:
# #From https://github.com/sekka1/Dockerfile-Ubuntu-Gnome/blob/master/Dockerfile
# #https://github.com/sekka1/Dockerfile-Ubuntu-Gnome



# # # Installing fuse filesystem is not possible in docker without elevated priviliges
# # # but we can fake installling it to allow packages we need to install for GNOME
# # RUN apt-get install libfuse2 -y && \
# # 	cd /tmp ; apt-get download fuse && \
# # 	cd /tmp ; dpkg-deb -x fuse_* . && \
# # 	cd /tmp ; dpkg-deb -e fuse_* && \
# # 	cd /tmp ; rm fuse_*.deb && \
# # 	cd /tmp ; echo -en '#!/bin/bash\nexit 0\n' > DEBIAN/postinst && \
# # 	cd /tmp ; dpkg-deb -b . /fuse.deb && \
# # 	cd /tmp ; dpkg -i /fuse.deb


# # # Upstart and DBus have issues inside docker.
# # RUN dpkg-divert --local --rename --add /sbin/initctl && ln -sf /bin/true /sbin/initctl

# # # Install GNOME and tightvnc server. These are old packages, signiture verificatio might fail.
# # RUN apt-get --allow-unauthenticated update && apt-get --allow-unauthenticated install -y xorg gnome-core gnome-session-flashback tightvncserver


# # # Pull in the hack to fix keyboard shortcut bindings for GNOME 3 under VNC
# # ADD https://raw.githubusercontent.com/sekka1/Dockerfile-Ubuntu-Gnome/master/gnome-keybindings.pl /usr/local/etc/gnome-keybindings.pl
# # RUN chmod +x /usr/local/etc/gnome-keybindings.pl

# # # Add the script to fix and customise GNOME for docker
# # ADD https://raw.githubusercontent.com/sekka1/Dockerfile-Ubuntu-Gnome/master/gnome-docker-fix-and-customise.sh /usr/local/etc/gnome-docker-fix-and-customise.sh
# # RUN chmod +x /usr/local/etc/gnome-docker-fix-and-customise.sh

# # # Set up VNC
# # RUN mkdir -p /root/.vnc
# # ADD https://raw.githubusercontent.com/sekka1/Dockerfile-Ubuntu-Gnome/master/xstartup /root/.vnc/xstartup
# # RUN chmod 755 /root/.vnc/xstartup
# # ADD https://raw.githubusercontent.com/sekka1/Dockerfile-Ubuntu-Gnome/master/spawn-desktop.sh /usr/local/etc/spawn-desktop.sh
# # RUN chmod +x /usr/local/etc/spawn-desktop.sh
# # RUN apt-get install -y expect
# # ADD https://raw.githubusercontent.com/sekka1/Dockerfile-Ubuntu-Gnome/master/start-vnc-expect-script.sh /usr/local/etc/start-vnc-expect-script.sh
# # RUN chmod +x /usr/local/etc/start-vnc-expect-script.sh
# # ADD https://raw.githubusercontent.com/sekka1/Dockerfile-Ubuntu-Gnome/master/vnc.conf /etc/vnc.conf


# # CMD bash -C '/usr/local/etc/spawn-desktop.sh';'bash'


# # #OPTION 2:
# # #Install GUI Environment
# # RUN apt-get install -y \
# # 	ubuntu-gnome-desktop \
# # 	x11vnc \
# # 	xvfb \
# # 	&& apt-get autoclean && apt-get autoremove -y
# # RUN apt-get -y install fuse  || :
# # RUN rm -rf /var/lib/dpkg/info/fuse.postinst
# # RUN apt-get -y install fuse fluxbox

# # #Set envirnmental variable for display	
# # ENV DISPLAY :1
# # #Expose port 5920 to view display using VNC Viewer
# # EXPOSE 5920
# # RUN dpkg-divert --local --rename --add /sbin/initctl && ln -sf /bin/true /sbin/initctl
# # RUN touch /usr/seed/entrypoint.sh
# # RUN echo "#!/bin/bash" >> /usr/seed/entrypoint.sh && \
# # 	echo "export DISPLAY=:20" >> /usr/seed/entrypoint.sh && \
	
# # 	echo "Xvfb :1 -screen 0 1024x768x16 &" >> /usr/seed/entrypoint.sh && \
# # 	echo "x11vnc -rfbport 5920 -passwd SeedDees -display :1 -N -forever &" >> /usr/seed/entrypoint.sh && \
# # 	echo "service dbus restart" >> /usr/seed/entrypoint.sh && \
# # 	echo "sleep 10" >> /usr/seed/entrypoint.sh && \
# # 	echo "gnome-session &" >> /usr/seed/entrypoint.sh
# # 	#echo "$SHELL" >> /usr/seed/entrypoint.sh
# # RUN chmod 755 /usr/seed/entrypoint.sh



# #OPTION 3: RDP
# EXPOSE 3389
# RUN apt-get install -yq xterm xrdp apt-utils sudo



# #ENTRYPOINT ["/usr/seed/entrypoint.sh"]
# #RUN apt-get install -y tightvncserver
# #CMD vncserver

# #RUN useradd seed -p $1$CtlkR27G$mWI0slk32wvOZkbdRQnZJ.
# #USER seed
# #ENV HOME /home/seed
# #CMD /usr/bin/gnome-builder
# #Enable Web VNC Desktop Environment







# #Install Sublime
# #RUN wget -qO - https://download.sublimetext.com/sublimehq-pub.gpg | sudo apt-key add - && \
# #	echo "deb https://download.sublimetext.com/ apt/stable/" | sudo tee /etc/apt/sources.list.d/sublime-text.list && \
# #	apt-get update && apt-get install -y sublime-text

# #WORKDIR /seed-setup/customization
# #RUN wget -P ./Files/ http://www.cis.syr.edu/~wedu/seed/Documentation/Ubuntu12_04_VM/Customization.zip
