#!/bin/bash

#Add Entires in /etc/hosts
if grep -q "127.0.0.1       User" /etc/hosts; then
    echo "User local host already set" 
else
    sudo sh -c "echo '127.0.0.1       User' >> /etc/hosts"
fi

if grep -q "127.0.0.1       Attacker" /etc/hosts; then
    echo "Attacker local host already set" 
else
    sudo sh -c "echo '127.0.0.1       Attacker' >> /etc/hosts"
fi

if grep -q "127.0.0.1       Server" /etc/hosts; then
    echo "Server local host already set" 
else
    sudo sh -c "echo '127.0.0.1       Server' >> /etc/hosts"
fi

