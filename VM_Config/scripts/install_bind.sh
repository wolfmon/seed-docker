#!/bin/bash

# DNS
apt-get install -y -qq bind9
apt-get install -y -qq libnet1-dev

# BIND config

cp ../Files/named.conf.options /etc/bind
chown bind /etc/bind/named.conf.options
touch /var/cache/bind/dump.db
chown bind /var/cache/bind/dump.db
chgrp bind /var/cache/bind/dump.db
service bind9 restart

