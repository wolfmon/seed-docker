#!/bin/bash

echo 
echo "================================"
echo "Installing SQL Injection Patch"
echo "================================"
echo 

chmod a+x ../Files/sql_injection_patch/bootstrap.sh
echo | ../Files/sql_injection_patch/bootstrap.sh


echo "================================"
echo "Installing lab specific tools"
echo "================================"
echo

# Shellnoob - assist in writing shellcode
git clone --recursive https://github.com/reyammer/shellnoob.git /home/seed/source/shellnoob

echo 
echo "================================"
echo "Installing ROP Lab Tools"
echo "================================"
echo

cwd=$(pwd)

#ROPgadget
apt-get -y install python-pip
yes | pip install capstone
git clone https://github.com/JonathanSalwan/ROPgadget.git /home/seed/source/ropgadget
cd /home/seed/source/ropgadget
python setup.py install

cd $cwd

#gdb-peda
cwd=$(pwd)
git clone https://github.com/longld/peda.git /home/seed/source/gdbpeda
echo "source /home/seed/source/gdbpeda/peda.py" >> ~/.gdbinit

cd $cwd

echo 
echo "================================"
echo "Installing C++ Boost"
echo "================================"
echo

cwd=$(pwd)

mkdir /home/seed/source
wget -P /home/seed/source/ https://dl.bintray.com/boostorg/release/1.64.0/source/boost_1_64_0.tar.bz2
cd /home/seed/source/
tar --bzip2 -xvf boost_1_64_0.tar.bz2
cd boost_1_64_0/
./bootstrap.sh --prefix=/home/seed/source/boost_1_64_0/
./b2
rm /home/seed/source/boost_1_64_0.tar.bz2

cd $cwd


cwd=$(pwd)

mkdir /home/seed/source/md5tool
wget -P /home/seed/source/md5tool https://www.win.tue.nl/hashclash/fastcoll_v1.0.0.5-1_source.zip
unzip /home/seed/source/md5tool/fastcoll_v1.0.0.5-1_source.zip -d /home/seed/source/md5tool/md5collgen/
cd /home/seed/source/md5tool/md5collgen
g++ -c md5.cpp
g++ -c block0.cpp
g++ -c block1.cpp
g++ -c block1stevens00.cpp
g++ -c block1stevens01.cpp
g++ -c block1stevens11.cpp
g++ -c block1stevens10.cpp
g++ -c block1wang.cpp
export LD_LIBRARY_PATH=/home/seed/source/boost_1_64_0/stage/lib:$LD_LIBRARY_PATH
g++ -o md5collgen main.cpp -I /home/seed/source/boost_1_64_0 -L /home/seed/source/boost_1_64_0/stage/lib -lboost_thread -lboost_serialization -lboost_program_options -lboost_filesystem -lboost_iostreams -lboost_system md5.o block0.o block1.o block1stevens00.o block1stevens01.o block1stevens10.o block1stevens11.o block1wang.o
rm /home/seed/source/md5tool/fastcoll_v1.0.0.5-1_source.zip

mkdir -p /home/seed/bin/md5tool/
cp /home/seed/source/md5tool/md5collgen/md5collgen /home/seed/bin/md5tool/

var=/home/seed/source/boost_1_64_0/stage/lib:$LD_LIBRARY_PATH
str="LD_LIBRARY_PATH="
quote="\""
str2=$str$quote$var$quote
export str2
sudo -E bash -c 'echo -e "$str2" >> /etc/environment'


cd $cwd

echo 
echo "================================"
echo "Change bashrc"
echo "================================"
echo

cp ../Files/bashrc ~/.bashrc