#!/bin/bash

echo "Configuring Host..."
/bin/sh config_hosts.sh 
#wait
echo "Done!"

echo "Installing Hex Editor..."
/bin/sh install_hex_editor.sh 
#wait
echo "Done!"

echo "Installing SSL Libraries..."
/bin/sh install_ssl_lib.sh 
#wait
echo "Done!"

echo "Installing Bind Packages..."
/bin/sh install_bind.sh 
#wait
echo "Done!"

echo "Installing LAMP Stack..."
/bin/sh install_lamp.sh 
#wait
echo "Done!"

echo "Installing Wireshark..."
/bin/sh install_wireshark.sh 
#wait
echo "Done!"

echo "Installing Lab Specific Dependencies"
/bin/sh install_external.sh 
echo "Done!"

echo "Install Elgg"
/bin/sh install_elgg.sh
echo "Done!"